import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as path from "path";

export class CdkappStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const imageFn = new cdk.aws_lambda.DockerImageFunction(this, 'ImageFn', {
      functionName: "image-fn",
      code: cdk.aws_lambda.DockerImageCode.fromImageAsset(path.join(__dirname, "fn"), {
      }),
    });
    new cdk.CfnOutput(this,'ImageFnName', {
      value: imageFn.functionName
    });
    new cdk.CfnOutput(this, 'ImageFnArn', {
      value: imageFn.functionArn
    });

  }
}
